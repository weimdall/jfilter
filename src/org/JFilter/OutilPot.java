package org.JFilter;

import java.awt.Color;
import java.awt.image.BufferedImage;

import javax.swing.JOptionPane;

public class OutilPot extends Outil
{
	BufferedImage save; 
	Color couleur;
	int seuil = 50;
	boolean[][] cache;
	
	@Override
	public void Appliquer(BufferedImage img)
	{
		return;
	}

	@Override
	public void Appliquer(BufferedImage img, int x, int y, Filtre f, int taille)
	{
		this.save = Outil.clone(img);
		this.seuil = (taille*255)/100;
		BufferedImage imgFiltre = Outil.clone(img);
		f.Appliquer(imgFiltre);
		this.couleur = new Color(this.save.getRGB(x, y));
		
		cache = new boolean[img.getWidth()][img.getHeight()];
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
				cache[i][j] = false;
		try
		{
			AppliquerFiltreSurPixel(x, y, img, imgFiltre);
		}
		catch (Exception e1)
		{
			
			JOptionPane.showMessageDialog(null,  "Overflow", "Message d'erreur", JOptionPane.ERROR_MESSAGE);
			
		}
		this.cache = null;
	}
	
	private void AppliquerFiltreSurPixel(int x, int y, BufferedImage imgBase, BufferedImage imgFiltre)
	{
		if(x < 0 || x >= imgBase.getWidth() || y < 0 || y >= imgBase.getHeight() || cache[x][y])
			return;
		
		if(((imgBase.getRGB(x, y) >> 16) & 0xFF) >= this.couleur.getRed()-this.seuil   &&
		   ((imgBase.getRGB(x, y) >> 8) & 0xFF) >= this.couleur.getGreen()-this.seuil  &&
	     	(imgBase.getRGB(x, y) & 0xFF) >= this.couleur.getBlue()-this.seuil         &&
	       ((imgBase.getRGB(x, y) >> 16) & 0xFF) <= this.couleur.getRed()+this.seuil   &&
		   ((imgBase.getRGB(x, y) >> 8) & 0xFF) <= this.couleur.getGreen()+this.seuil  &&
		   	(imgBase.getRGB(x, y) & 0xFF) <= this.couleur.getBlue()+this.seuil)
		{
			cache[x][y] = true;
			imgBase.setRGB(x, y, imgFiltre.getRGB(x, y));
			this.AppliquerFiltreSurPixel(x+1, y, imgBase, imgFiltre);
			this.AppliquerFiltreSurPixel(x-1, y, imgBase, imgFiltre);
			this.AppliquerFiltreSurPixel(x, y+1, imgBase, imgFiltre);
			this.AppliquerFiltreSurPixel(x, y-1, imgBase, imgFiltre);
		}
	}
	
	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		img.setData(this.save.getData());
		return true;
	}
}
