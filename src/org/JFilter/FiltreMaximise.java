package org.JFilter;

import java.awt.image.BufferedImage;

public class FiltreMaximise implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		int intrgb = 0, rgb[] = {0, 0, 0}, max[] = {0, 0, 0}, min[] = {255, 255, 255};
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{
				intrgb = img.getRGB(i, j);
				
				rgb[0] = (intrgb >> 16) & 0xFF;
				rgb[1] = (intrgb >> 8) & 0xFF;
				rgb[2] = intrgb & 0xFF;
				
				
				for(int k = 0 ; k < 3 ; k++)
				{
					if(rgb[k] < min[k])
						min[k] = rgb[k];
					if(rgb[k] > max[k])
						max[k] = rgb[k];
				}
			}
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{
				intrgb = img.getRGB(i, j);
				
				rgb[0] = (intrgb >> 16) & 0xFF;
				rgb[1] = (intrgb >> 8) & 0xFF;
				rgb[2] = intrgb & 0xFF;
				
				rgb[0] = ((rgb[0]-min[0])*255)/(max[0]-min[0]);
				rgb[1] = ((rgb[1]-min[1])*255)/(max[1]-min[1]);
				rgb[2] = ((rgb[2]-min[2])*255)/(max[2]-min[2]);
				
				intrgb = rgb[0];
				intrgb = (intrgb << 8) + rgb[1];
				intrgb = (intrgb << 8) + rgb[2];
				
				img.setRGB(i, j, intrgb);
			}
		
	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
