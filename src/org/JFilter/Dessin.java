package org.JFilter;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Dessin extends JPanel implements MouseMotionListener, MouseListener
{
	private JScrollPane scrollPane;
	private BufferedImage image;
	private File fichierImage;
	private ArrayList<Filtre> filtres;
	private Outil outil = null;
	private Filtre filtreCourant;
	private int seuil;	
	private double zoom;
	private boolean click = false;
	
	public Dessin()
	{
		super();
		
		this.setSize(500, 500);
		this.zoom = 1;
		this.seuil = 20;
		scrollPane = new JScrollPane(this);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
		image = null;
		filtres = new ArrayList<Filtre>();
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setOutil(new OutilPinceau());
		this.setFiltreCourant(new FiltreNuanceGris());
	}
	
	public void Actualiser()
	{
		this.setPreferredSize(new Dimension((int)(this.image.getWidth()*this.zoom), (int)(this.image.getHeight()*this.zoom)));
		this.repaint();
		this.revalidate();
		this.scrollPane.revalidate();
	}
	
	public void setZoom(double zoom)
	{
		this.zoom += zoom;
		this.Actualiser();
	}
	
	public void setSeuil(int s)
	{
		this.seuil = s;
	}
	
	public int getSeuil()
	{
		return this.seuil;
	}
	
	public BufferedImage getImage()
	{
		return image;
	}
	
	public JScrollPane getScrollPane()
	{
		return scrollPane;
	}
	
	public void setFiltreCourant(Filtre f)
	{
		this.filtreCourant = f;
	}
	
	public Filtre getFiltreCourant()
	{
		return this.filtreCourant;
	}
	
	public void setOutil(Outil o)
	{
		this.outil = o;
	}
	
	public Outil getOutil(Outil o)
	{
		return this.outil;
	}
	
	
	public void setImage(File fichier) throws IOException
	{
		this.fichierImage = fichier;
		this.image = ImageIO.read(fichier);
		
		this.zoom = 1;
		
		this.setPreferredSize(new Dimension((int)(this.image.getWidth()*this.zoom), (int)(this.image.getHeight()*this.zoom)));
		
		this.Actualiser();
	}
	
	public void RechargerImage() throws IOException
	{
		if(this.image == null)
			return;
		
		this.setImage(this.fichierImage);
	}
	
	public void AjouterFiltre(Filtre f)
	{
		f.Appliquer(this.image);
		this.filtres.add(f);
		this.Actualiser();
	}
	
	public void AnnulerDernierFiltre()
	{
		int last = this.filtres.size()-1;
		if(last < 0)
			return;
		
		Filtre filtre = this.filtres.get(last);
		this.filtres.remove(last);
		if(!filtre.AnnulerFiltre(this.image))
		{
			try
			{
				this.RechargerImage();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			for(int i = 0 ; i < this.filtres.size() ; i++)
				this.filtres.get(i).Appliquer(this.image);
		}
		
		this.Actualiser();
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		this.setBackground(Color.WHITE);
		if(image != null)
			g.drawImage(image, 0, 0, (int)(this.image.getWidth()*zoom), (int)(this.image.getHeight()*zoom), this);
		
	}
	


	@Override
	public void mouseClicked(MouseEvent e)
	{
		if(e.getX()/this.zoom > this.image.getWidth() || e.getY()/this.zoom > this.image.getHeight() || this.outil == null)
			return;
		
		try
		{
			Outil o = this.outil.getClass().newInstance();
			o.Appliquer(this.image, (int)(e.getX()/zoom), (int)(e.getY()/zoom), this.filtreCourant, this.seuil);
			this.filtres.add(o);
			this.Actualiser();
		} catch (InstantiationException | IllegalAccessException e1)
		{
			e1.printStackTrace();
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		if(this.outil == null)
			return;
		
		try
		{
			Outil o = this.outil.getClass().newInstance();
			o.Appliquer(this.image, (int)(e.getX()/zoom), (int)(e.getY()/zoom), this.filtreCourant, this.seuil);
			this.filtres.add(o);
			this.Actualiser();
		} catch (InstantiationException | IllegalAccessException e1)
		{
			e1.printStackTrace();
		}
		this.click = true;
		
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		this.click = false;
		
	}

	@Override
	public void mouseDragged(MouseEvent e)
	{
		if(this.click)
		{
			Outil o = (Outil)this.filtres.get(this.filtres.size()-1);
			o.Appliquer(this.image, (int)(e.getX()/zoom), (int)(e.getY()/zoom), this.filtreCourant, this.seuil);
			this.Actualiser();
		}
		
	}

	@Override
	public void mouseMoved(MouseEvent e)
	{
		
			
		
	}
}
