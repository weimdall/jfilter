package org.JFilter;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Fenetre extends JFrame implements ActionListener, ChangeListener
{
	private JTabbedPane onglets;
	private ArrayList<Dessin> listeDessin;
	
	private Choix boutons;
	
	private String[] nomsFiltres = {"Nuances de gris", "Maximiser", "Negatif", "Cryptage", "Decryptage", "Flou gaussien", "Assombrir", "Eclaircir", "Saturation", "Filtre Noir"};
	private Filtre[] filtres = {new FiltreNuanceGris(), new FiltreMaximise(), new FiltreNegatif(), new FiltreCryptage(), new FiltreDecryptage(), new FiltreFlou(), new FiltreAssombrir(), new FiltreEclaircir(), new FiltreSaturation(), new FiltreNoir()};
	private String[] iconesBoutons = {"./png/brush24.png", "./png/pencil112.png", "./png/write61.png", "./png/graffiti.png", "./png/paint.png", "./png/spray14.png", "./png/paint82.png", "./png/fish2.png", "./png/bucket15.png", "./png/paint4.png", "./png/zoom71.png", "./png/zoom72.png"};
	private String[] menus = {"Fichier", "Edition", "Filtres", "A propos"};
	private String[][] sousMenu = {{"Nouveau", "Ouvrir ...", "Sauvegarder ...", "Fermer", "", "Quitter"}, {"Annuler"}, nomsFiltres};
	private String[][] iconesSousMenu = {{"./png/text150.png", "./png/open131.png", "./png/floppy13.png", "./png/delete30.png", null, "./png/door13.png"}, {"./png/arrowheads3.png"}};
	private JSlider slider;
	
	public Fenetre(String titre, int x, int y, int width, int height) throws Exception
	{
		super(titre);
		this.setLocation(x,  y);
		this.setSize(width, height);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		
		this.onglets = new JTabbedPane();
		this.listeDessin = new ArrayList<Dessin>();
		this.listeDessin.add(new Dessin());
		this.onglets.addTab("Vide", this.listeDessin.get(0).getScrollPane());
		
		this.slider = new JSlider(JSlider.HORIZONTAL, 0, 100, this.listeDessin.get(0).getSeuil());
		this.slider.addChangeListener(this);
		
		JPanel panelSouth = new JPanel();
		boutons = new ChoixIcons(iconesBoutons);
		panelSouth.setLayout(new BorderLayout());
		panelSouth.add(boutons, "West");
		panelSouth.add(slider, "East");
		
		MenuFenetre menu = new MenuFenetre(menus);
		menu.addSousMenu(menus[0], sousMenu[0], iconesSousMenu[0]);
		menu.addSousMenu(menus[1], sousMenu[1], iconesSousMenu[1]);
		menu.addSousMenu(menus[2], sousMenu[2]);
		
		Container contentPane = this.getContentPane();
		contentPane.add(onglets, BorderLayout.CENTER);
		contentPane.add(panelSouth, BorderLayout.SOUTH);
		contentPane.add(menu.getMenuBar(), BorderLayout.NORTH);
		
		
		//Add listener
		
		for(int i = 0 ; i < iconesBoutons.length ; i++)
			boutons.getBouton(i).addActionListener(this);
		
		
		for(int i = 0 ; i < menus.length ; i++)
		{
			menu.getMenu(i).addActionListener(this);
			for(int j = 0 ; j < menu.getMenu(i).getItemCount() ; j++)
			{
				if(menu.getMenu(i).getItem(j) != null)
					menu.getMenu(i).getItem(j).addActionListener(this);
			}
		}
		
		this.setVisible(true);
		
	}
	@Override
	public void actionPerformed(ActionEvent e)
	{
		String cmd = e.getActionCommand();
		Dessin dessin = this.listeDessin.get(onglets.getSelectedIndex());
		
		for(int i = 0 ; i < this.nomsFiltres.length ; i++)
		{
			if(cmd.equals(this.nomsFiltres[i]))
				dessin.AjouterFiltre(this.filtres[i]);
		}
		
		if(cmd.equals(this.sousMenu[0][5]))
			System.exit(0);
		else if(cmd.equals(this.sousMenu[0][1]))
		{
			JFileChooser dialogue = new JFileChooser(new File("."));
			File fichier;
			
			if(dialogue.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
			{
			    fichier = dialogue.getSelectedFile();
			    try
				{
					dessin.setImage(fichier);
					this.onglets.setTitleAt(this.onglets.getSelectedIndex(), fichier.getName());
					
				} catch (Exception e1)
				{
					JOptionPane.showMessageDialog(null,  "Importation impossible.\nLe fichier sélectionné ne semble pas être une image.", "Message d'erreur",JOptionPane.ERROR_MESSAGE);
				}
			    
			}
		}
		else if(cmd.equals(this.sousMenu[0][2]))
		{
			JFileChooser dialogue = new JFileChooser(new File("."));
			File fichier;
			
			if(dialogue.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			{
			    fichier = dialogue.getSelectedFile();
			    try
				{
					ImageIO.write(dessin.getImage(), "png", fichier);
				} catch (IOException e1)
				{
					e1.printStackTrace();
				}
			    
			}
		}
		else if(cmd.equals(this.sousMenu[0][0]))
		{
			this.listeDessin.add(new Dessin());
			this.listeDessin.get(this.listeDessin.size()-1).setSeuil(this.slider.getValue());
			this.onglets.addTab("Vide", this.listeDessin.get(this.listeDessin.size()-1).getScrollPane());
			this.onglets.setSelectedIndex(this.onglets.getTabCount()-1);
		}
		else if(cmd.equals(this.sousMenu[0][3]))
		{
			this.listeDessin.remove(this.onglets.getSelectedIndex());
			this.onglets.remove(this.onglets.getSelectedIndex());
			if(this.onglets.getTabCount() <= 0)
			{
				this.listeDessin.add(new Dessin());
				this.onglets.addTab("Vide", this.listeDessin.get(this.listeDessin.size()-1).getScrollPane());
			}
			
		}
		else if(cmd.equals(this.sousMenu[1][0]))
			dessin.AnnulerDernierFiltre();

		else
		{	
			for(int i = 0 ; i < this.iconesBoutons.length ; i++)
			{
				if(e.getSource() == this.boutons.getBouton(i))
				{
					switch(i)
					{
						case 0:
							dessin.setOutil(new OutilPinceau());
							break;
						case 1:
							dessin.setOutil(new OutilCrayon());
							break;
							
						case 8:
							dessin.setOutil(new OutilPot());
							break;
							
						case 9:
							String filtre = (String)JOptionPane.showInputDialog(null, "Choisisser le filtre courant :", "Choix du filtre", JOptionPane.PLAIN_MESSAGE, null, this.nomsFiltres, this.nomsFiltres[0]);
							for (int j = 0 ; j < this.nomsFiltres.length ; j++)
								if(this.nomsFiltres[j].equals(filtre))
									dessin.setFiltreCourant(this.filtres[j]);
							break;
							
						case 10:
							dessin.setZoom(-0.1);
							break;
							
						case 11:
							dessin.setZoom(0.1);
							break;

						default:
							System.out.println("Action non taité : " + cmd);
							break;
					}
				}
			}
		}
		
		return;
	}
	
	@Override
	public void stateChanged(ChangeEvent arg0)
	{
		if(arg0.getSource() == this.slider)
			for(int i = 0 ; i < this.listeDessin.size() ; i++)
				this.listeDessin.get(i).setSeuil(this.slider.getValue());
		
	}
	
	public static void main(String[] args)
	{
		
		try
		{
			Fenetre fenetre = new Fenetre("JDraw", 0, 0, 780, 500);
		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	
}
