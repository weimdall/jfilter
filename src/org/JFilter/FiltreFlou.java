package org.JFilter;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

public class FiltreFlou implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		BufferedImage imgCopy = FiltreFlou.clone(img);
		int intrgb = 0, moy[] = {0,0,0}, somme = 0;
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{				
				moy[0] = 0;
				moy[1] = 0;
				moy[2] = 0;
				somme = 0;
				
				
				for(int k = i-1 ; k <= i+1 ; k++)
					for(int l = j-1 ; l <= j+1 ; l++)
					{
						if(k < 0 || k >= img.getWidth() || l < 0 || l >= img.getHeight())
							continue;
						
						intrgb = imgCopy.getRGB(k, l);
						moy[0] += (intrgb >> 16) & 0xFF;
						moy[1] += (intrgb >> 8) & 0xFF;
						moy[2] += intrgb & 0xFF;
						somme++;
					}
				
				moy[0] /= somme;
				moy[1] /= somme;
				moy[2] /= somme;
				
				intrgb = moy[0];
				intrgb = (intrgb << 8) + moy[1];
				intrgb = (intrgb << 8) + moy[2];
				
				img.setRGB(i, j, intrgb);
			}

	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	
	//-------------------------------------------------------------------------
	// Fonction trouvé sur internet pour cloner une instance de BufferedImage
	//-------------------------------------------------------------------------
	static BufferedImage clone(BufferedImage bi)
	{
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	// ------------------------------------------------------------------------

}
