package org.JFilter;

import java.awt.image.BufferedImage;

public class FiltreCryptage implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		
		this.Cryptage(img, true, 0);

	}
	
	private void Cryptage(BufferedImage img, boolean cryptage, int etage)
	{
		if(etage >= 50)
			return;
		int rgb = 0;
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{
				rgb = img.getRGB(i, j);
				
				if(cryptage)
					rgb += (i*j*etage);
				else
					rgb -= (i*j*etage);
				
				img.setRGB(i, j, rgb);
			}
		this.Cryptage(img, cryptage, etage+1);
		
	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		this.Cryptage(img, false, 0);
		return true;
	}

}
