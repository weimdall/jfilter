package org.JFilter;

import java.awt.image.BufferedImage;

public class FiltreNegatif implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		int rgb = 0;
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{
				rgb = img.getRGB(i, j);
				
				rgb = -rgb;
				
				img.setRGB(i, j, rgb);
			}

	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		this.Appliquer(img);
		return true;
	}

}
