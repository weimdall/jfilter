package org.JFilter;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class Choix extends JPanel
{
	protected JButton[] boutons;
	protected int nombre;	
	
	public Choix()
	{
		super();
	}
	
	public Choix(String[] nom, int rows, int cols)
	{
		super();
		boutons = new JButton[nom.length];
		nombre = nom.length;
		this.setLayout(new GridLayout(rows, cols));
		
		for(int i = 0 ; i < nombre ; i++)
		{
			boutons[i] = new JButton(nom[i]);
			this.add(boutons[i]);
		}
	}
	
	public JButton getBouton(int i)
	{
		if(i >= 0 && i < this.nombre)
			return boutons[i];
		
		return null;
	}
}
