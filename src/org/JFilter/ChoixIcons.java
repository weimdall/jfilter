package org.JFilter;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ChoixIcons extends Choix
{
	protected Icon[] icones;
	
	public ChoixIcons(String[] cheminIcones) throws Exception
	{
		this.nombre = cheminIcones.length;
		this.boutons = new JButton[this.nombre];
		this.setLayout(new GridLayout(2,2));
		
		this.icones = new ImageIcon[this.nombre];
		
		for(int i = 0 ; i < this.nombre ; i++)
		{
			this.icones[i] = new ImageIcon(cheminIcones[i]);
			this.boutons[i] = new JButton(this.icones[i]);
			this.add(boutons[i]);
		}
				
		
	}
}
