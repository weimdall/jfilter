package org.JFilter;
import java.awt.image.BufferedImage;

public class FiltreNuanceGris implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		int rgb = 0, r = 0, g = 0, b = 0, moy = 0;
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{
				rgb = img.getRGB(i, j);
				
				r = (rgb >> 16) & 0xFF;
				g = (rgb >> 8) & 0xFF;
				b = rgb & 0xFF;
				
				moy = (r+g+b)/3;
				
				rgb = moy;
				rgb = (rgb << 8) + moy;
				rgb = (rgb << 8) + moy;
				
				img.setRGB(i, j, rgb);
			}
	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		// TODO Auto-generated method stub
		return false;
	}
	
}
