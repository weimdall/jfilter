package org.JFilter;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

public abstract class Outil implements Filtre
{
	public abstract void Appliquer(BufferedImage img, int x, int y, Filtre f, int taille);
	
	//-------------------------------------------------------------------------
	// Fonction trouvé sur internet pour cloner une instance de BufferedImage
	//-------------------------------------------------------------------------
	static BufferedImage clone(BufferedImage bi)
	{
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	// ------------------------------------------------------------------------
}
