package org.JFilter;

import java.awt.image.BufferedImage;

public class FiltreNoir implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		int rgb = 0, r = 0, g = 0, b = 0;
		int rgbMax = -((255*(256*256))+(255*256)+255);
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
			{
				rgb = img.getRGB(i, j);
				
				r = (rgb >> 16) & 0xFF;
				g = (rgb >> 8) & 0xFF;
				b = rgb & 0xFF;
				
				r = 0;
				g = 0;
				b = 0;
				
				rgb = r;
				rgb = (rgb << 8) + g;
				rgb = (rgb << 8) + b;
				
				img.setRGB(i, j, rgb);
			}

	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
