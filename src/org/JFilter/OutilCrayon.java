package org.JFilter;

import java.awt.image.BufferedImage;

public class OutilCrayon extends Outil
{
	BufferedImage save = null;
	boolean[][] cache;
		
	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		img.setData(this.save.getData());
		return true;
	}

	@Override
	public void Appliquer(BufferedImage img, int x, int y, Filtre f, int taille)
	{
		if(this.save == null)
			this.save = Outil.clone(img);
		taille /= 10;
		
		BufferedImage imgFiltre = Outil.clone(this.save);
		f.Appliquer(imgFiltre);
		
		cache = new boolean[img.getWidth()][img.getHeight()];
		for(int i = x-taille ; i < x+taille ; i++)
			for(int j = y-taille ; j < y+taille ; j++)
				if(i >= 0 && j >= 0 && i < img.getWidth() && j < img.getHeight())
					cache[i][j] = true;
		
		for(int i = 0 ; i < img.getWidth() ; i++)
			for(int j = 0 ; j < img.getHeight() ; j++)
				if(cache[i][j])
					img.setRGB(i, j, imgFiltre.getRGB(i, j));
		
	}

	@Override
	public void Appliquer(BufferedImage img)
	{
		// TODO Auto-generated method stub
		
	}

}
