package org.JFilter;

import java.awt.Color;

public class ChoixCouleur extends Choix
{
	public ChoixCouleur(String[] nom, Color[] couleurs, int rows, int cols) throws Exception
	{
		super(nom, rows, cols);
		
		if(nom.length != couleurs.length)
			throw new Exception("Erreur de paramètres");
		
		for(int i = 0 ; i < this.nombre ; i++)
			boutons[i].setBackground(couleurs[i]);
		
	}
}
