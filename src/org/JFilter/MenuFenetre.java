package org.JFilter;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuFenetre
{
	private JMenuBar menuBar = new JMenuBar();
	private JMenu[] menu;
	private int taille;
	
	public MenuFenetre(String[] noms)
	{
		menu = new JMenu[noms.length];
		taille = noms.length;
		
		for(int i = 0 ; i < taille ; i++)
		{
			menu[i] = new JMenu(noms[i]);
			menuBar.add(menu[i]);
		}
	}
	
	public void addSousMenu(String nom, String[] noms)
	{
		int taille = noms.length;
		int index = -1;
		
		for(int i = 0 ; i < this.taille ; i++)
			if(menu[i].getText().equals(nom))
				index = i;
		
		if(index == -1)
			return;
		
		for(int i = 0 ; i < taille ; i++)
		{
			if(noms[i].equals(""))
				menu[index].addSeparator();
			else
				menu[index].add(new JMenuItem(noms[i]));
		}
	}
	
	public void addSousMenu(String nom, String[] noms, String[] icones)
	{
		if(noms.length != icones.length)
			this.addSousMenu(nom, noms);
		
		int taille = noms.length;
		int index = -1;
		
		for(int i = 0 ; i < this.taille ; i++)
			if(menu[i].getText().equals(nom))
				index = i;
		
		if(index == -1)
			return;
		
		for(int i = 0 ; i < taille ; i++)
		{
			if(noms[i].equals(""))
				menu[index].addSeparator();
			else
				menu[index].add(new JMenuItem(noms[i], new ImageIcon(icones[i])));
		}
	}
	
	public JMenuBar getMenuBar()
	{
		return menuBar;
	}
	public JMenu getMenu(int i)
	{
		if(i >= 0 && i < this.taille)
			return menu[i];
		return null;
	}
}
