package org.JFilter;

import java.awt.image.BufferedImage;

public class FiltreDecryptage implements Filtre
{

	@Override
	public void Appliquer(BufferedImage img)
	{
		FiltreCryptage f = new FiltreCryptage();
		f.AnnulerFiltre(img);

	}

	@Override
	public boolean AnnulerFiltre(BufferedImage img)
	{
		FiltreCryptage f = new FiltreCryptage();
		f.Appliquer(img);
		return true;
	}

}
