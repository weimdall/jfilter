package org.JFilter;
import java.awt.image.BufferedImage;

public interface Filtre
{
	public void Appliquer(BufferedImage img);
	public boolean AnnulerFiltre(BufferedImage img);
}

	