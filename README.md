      _     ___     _       _      _                     
   _ | |   | __|   (_)     | |    | |_     ___      _ _  
  | || |   | _|    | |     | |    |  _|   / -_)    | '_| 
  _\__/   _|_|_   _|_|_   _|_|_   _\__|   \___|   _|_|_  
_|"""""|_| """ |_|"""""|_|"""""|_|"""""|_|"""""|_|"""""| 
"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-'"`-0-0-' 


Ecrit par Julien Cassagne
JFilter est un programme écrit en Java permettant l'application de filtre sur des images.

						  ___
						 / \ \
						/ /_\_\
						\/____/


Les sources se trouve dans le package org.JFilter
La compilation doit s'effectuer à la racine du projet

						  ___
						 / \ \
						/ /_\_\
						\/____/

L'utilisation de JFilter est simple :
    * Les filtres peuvent être appliquer sur toute l'image vie le menu Filtres.
    * Les filtres peuvent également être appliqué localement via les outils mit à disposition.


         _________________________.
        / _____________________  /|
       / / ___________________/ / |
      / / /| |               / /  |
     / / / | |              / / . |
    / / /| | |             / / /| |
   / / / | | |            / / / | |
  / / /  | | |           / / /| | |
 / /_/___| | |__________/ / / | | |
/________| | |___________/ /  | | |
| _______| | |__________ | |  | | |
| | |    | | |_________| | |__| | |
| | |    | |___________| | |____| |
| | |   / / ___________| | |_  / /
| | |  / / /           | | |/ / /
| | | / / /            | | | / /
| | |/ / /             | | |/ /
| | | / /              | | ' /
| | |/_/_______________| |  /
| |____________________| | /
|________________________|/
